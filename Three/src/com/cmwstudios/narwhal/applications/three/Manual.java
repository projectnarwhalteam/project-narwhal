/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.applications.three;

//import com.cmwstudios.hiya.jash.JASH;
//import com.cmwstudios.hiya.jash.PrivilageLevel;
import com.cmwstudios.hiya.jash.JASH;
import com.cmwstudios.hiya.jash.PrivilageLevel;
import com.cmwstudios.narwhal.ANSI;
import com.cmwstudios.narwhal.Narwhal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Manual implements ANSI {
    public static void main(String args[]) {
        try {
            prompt(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void prompt(boolean loop) throws IOException {
        if (Narwhal.stopping) {
            return;
        }
        if (!loop) {
            System.out.println("Manual Pages");
        }
//        System.out.println("I made it, whew!");
//        System.out.append(dir + " $ ");
        System.out.print(ANSI_RED + "Manual" + ANSI_RESET + " > ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String l = br.readLine();
        try {
            //noinspection ResultOfMethodCallIgnored
            Short.parseShort(l);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input: " + l);
            prompt(true);
        }
        short s = Short.parseShort(l);
//        Short s = Short.parseShort(br.readLine());
        if (s == 0) {
            System.out.println("Returns to the Software Menu (NARWHAL prompt). If typed at Software Menu, will shut down Project Narwhal.");
            System.out.println("Type 255 to return from Manual.");
        } else if (s == 1) {
            System.out.println("Shuts down the system and boots it in recovery mode. Can only be done from the Software Menu (NARWHAL Prompt).");
        } else if (s == 255) {
            JASH.prompt("lol", PrivilageLevel.USER, false);
        }
//        String j = s.replace(dir + " $", "");
//        try {
//            JASHInterpreter jashInterpreter = new JASHInterpreter(s);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        sys(j);
//        sys("java -jar " + j);
        prompt(true);
    }
}
