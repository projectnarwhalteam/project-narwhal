RELEASE_STAGE=$1
if [ -z "$RELEASE_STAGE" ]; then
  RELEASE_STAGE="production"
fi

if [ -z "$BUGSNAG_API_KEY" ]; then
  echo "Bugsnag API key must be set"
  exit 1
fi

if [ "$BUGSNAG_AUTO_ASSIGN" = true ]; then
  AUTO_ASSIGN_RELEASE="true"
else
  AUTO_ASSIGN_RELEASE="false"
fi

if [ -z "$BUGSNAG_APP_VERSION" ]; then
  APP_VERSION=${BITBUCKET_BUILD_NUMBER}
else
  APP_VERSION=${BUGSNAG_APP_VERSION}
fi

curl https://build.bugsnag.com/ \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{\"apiKey\":\"${BUGSNAG_API_KEY}\",\"appVersion\":\"${APP_VERSION}\",\"releaseStage\":\"${RELEASE_STAGE}\",\"autoAssignRelease\":${AUTO_ASSIGN_RELEASE},\"buildTool\":\"bitbucket-pipelines\",\"sourceControl\":{\"repository\":\"https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}\",\"revision\":\"${BITBUCKET_COMMIT}\"}}"
