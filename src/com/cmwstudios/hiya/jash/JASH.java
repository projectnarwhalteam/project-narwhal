/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.hiya.jash;

import com.cmwstudios.narwhal.ANSI;
import com.cmwstudios.narwhal.Narwhal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JASH implements JASHSystem, ANSI {
    // --Commented out by Inspection (12/11/18 12:05 PM):public String dir;
    public static int crashCounter = 0;
    public static void main(String[] args) {
        print(releaseName);
        print(releaseCode);
// --Commented out by Inspection START (12/11/18 12:05 PM):
//        prompt("/home/aidan", PrivilageLevel.USER, false);
//    }
//
//    public static void sys(String s) {
//        String j;
//        Process p;
//        try {
//            p = Runtime.getRuntime().exec(s);
//            BufferedReader br = new BufferedReader(
//                    new InputStreamReader(p.getInputStream()));
//            while ((j = br.readLine()) != null)
//                System.out.println(j);
//            p.waitFor();
//        } catch (Exception e) { // PUSH PUSH PUSH OH YES!!!!!!!
// --Commented out by Inspection STOP (12/11/18 12:05 PM)
    }
// OH YES WALUIGI OH OH OH OH OH YES YES YES UGHHHHHHHHHH....!!!!!!!!!
private static void print(String s) {
    System.out.println(s);
}

    public static void prompt(String dir, PrivilageLevel priv, boolean loop) throws IOException {
        if (Narwhal.stopping) {
            return;
        }
        if (!loop) {
            System.out.println(ANSI_BLUE + "Software Menu" + ANSI_RESET);
            crashCounter++;
        }
//        System.out.append(dir + " $ ");
        System.out.append(ANSI_PURPLE).append("NARWHAL > ").append(ANSI_RESET);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String l = br.readLine();
        try {
            //noinspection ResultOfMethodCallIgnored,ResultOfMethodCallIgnored
            Short.parseShort(l);
        } catch (NumberFormatException e) {
                System.out.println("Invalid input: " + l);
            prompt("lol", PrivilageLevel.USER, true);
        }
        short s = Short.parseShort(l);
//        String j = s.replace(dir + " $", "");
//        print("test");
        JASHInterpreter jashInterpreter = new JASHInterpreter(s);
        print("test");
//        sys(j);
//        sys("java -jar " + j);
        prompt(dir, priv, true);
//        try {
//            throw new LigmaException();
//        } catch (LigmaException e) {
//            e.printStackTrace();
//        }
    }

}

// waluigi
