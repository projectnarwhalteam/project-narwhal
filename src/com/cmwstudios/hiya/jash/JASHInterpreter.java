/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.hiya.jash;

import com.bugsnag.Bugsnag;
import com.cmwstudios.narwhal.ANSI;
import com.cmwstudios.narwhal.Narwhal;
import com.cmwstudios.narwhal.Props;
import com.cmwstudios.narwhal.applications.three.Manual;
import com.cmwstudios.narwhal.boot.recovery.Recovery;
import com.cmwstudios.narwhal.boot.recovery.RecoveryReason;
import com.cmwstudios.narwhal.exception.OSHAViolation;
import com.cmwstudios.narwhal.settings.SystemSettings;
import com.cmwstudios.narwhal.shutdown.ShutdownJob;

import java.io.IOException;

class JASHInterpreter implements ANSI {
    //    private static Bugsnag bugsnag = new Bugsnag("07b75ed94b6e0b5e38ecae8da9069e3");
    Bugsnag bugsnag = new Bugsnag("07b75ed94b6e0b15e38ecae8da9069e3");

    public JASHInterpreter(short s) {
        if (s == 0) {
            Narwhal.jobs++;
            Narwhal.jobs--;
            try {
                ShutdownJob j = new ShutdownJob(true);
            } catch (InterruptedException e) {
                if (Props.getValue("AllowBugsnag")) {
                    bugsnag.notify(e);
                    System.out.println("Reported bug to Bugsnag.");
                }
                e.printStackTrace();
            }
        } else if (s == 1) {
            Recovery.rebootRecovery(RecoveryReason.USER);
        } else if (s == 2) {
            System.out.println("Based on JASH Alpha Series Version 1.1");
        } else if (s == 3) {
            Manual.main(null);
//            JASH.sys("java -jar " + s + ".jar");
        } else if (s == 101) {
            bugsnag.notify(new OSHAViolation());
        } else if (s == 4) {
            SystemSettings.main(null);
        }
        else {
            System.out.println(ANSI_RED + "Failed to execute instruction: " + s);
        }
    }
}