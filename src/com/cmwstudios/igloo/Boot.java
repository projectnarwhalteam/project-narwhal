/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.igloo;

import com.bugsnag.Bugsnag;

class Boot {
    public static void main(String args[]) {
        Bugsnag bugsnag = new Bugsnag("07b75ed94b6e0b15e38ecae8da9069e3");
        CMWSF.start();
        // yeet
//        bugsnag.notify(new OSHAViolation());
    }
}
// waluigi