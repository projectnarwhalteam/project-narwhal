/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.igloo;

//import com.cmwstudios.narwhal.ui.NarwhalGUI;

import com.bugsnag.Bugsnag;
import com.cmwstudios.hiya.jash.JASH;
import com.cmwstudios.hiya.jash.PrivilageLevel;

import java.io.IOException;
import java.time.LocalDateTime;

//import com.cmwstudios.igloo.statehandler.StateHandler;

public class CMWSF implements Framework {
    // --Commented out by Inspection (12/11/18 12:05 PM):public static SoftwareState state = SoftwareState.STOPPED;
    // --Commented out by Inspection (12/11/18 12:05 PM):public static boolean recovered = false;
    private static final int hour = LocalDateTime.now().getHour();

// --Commented out by Inspection START (12/11/18 12:05 PM):
//    public CMWSF() {
//    }
// --Commented out by Inspection STOP (12/11/18 12:05 PM)
static Bugsnag b = new Bugsnag("07b75ed94b6e0b15e38ecae8da9069e3");
    public static void start() {
        System.out.println("CMW Studios...");
        if (hour == 0) {
            System.out.println("It's a new day, let's make it a great one.");
        } else if (hour < 12) {
            System.out.println("Good Morning.");
        } else if (hour == 12) {
            System.out.println("It's high noon.");
        } else if (hour < 18) {
            System.out.println("Good afternoon!");
        } else if (hour < 21) {
            System.out.println("Good Evening.");
        } else if (hour > 21) {
            System.out.println("Good Night.");
        } else {
            System.out.println("Hi. Your clock is invalid. You should probably fix that. Or, maybe I'm broken? Or maybe it's Java? Maybe it's even a CMOS failure. Check your clock.");
        }
        System.out.println("Starting software " + softwareName + "...");
//        state = SoftwareState.STARTING;
        try {
            JASH.prompt("y", PrivilageLevel.USER, false);
        } catch (IOException e) {
            b.notify(e);
            e.printStackTrace();
        }
        //        NarwhalGUI.main(null);
    }
}
