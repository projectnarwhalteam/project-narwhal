/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.igloo;

enum Color {

    BROWN, YELLOW, BLACK, BLUE
}
