package com.cmwstudios.igloo;

class Puppy {
    public Puppy(String name, int age, Color color) {
        System.out.println(name + " is " + age + "years old and is " + color);
    }
}
