/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.igloo;

public enum SoftwareState {
  STOPPED, ABORTED, STARTING, RUNNING, STOPPING, STARTFAILED
}
