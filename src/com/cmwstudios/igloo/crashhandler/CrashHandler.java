/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.igloo.crashhandler;

import com.cmwstudios.igloo.CMWSF;
import com.cmwstudios.igloo.Framework;
import com.cmwstudios.igloo.SoftwareState;

public class CrashHandler implements Framework {
    public CrashHandler(Exception e, SoftwareState s) {
        System.out.println("Exception occurred. " + e.getMessage());
        System.out.println("------------BEGIN STACK TRACE----------------");
        e.printStackTrace();
        System.out.println("------------END STACK TRACE---------------");
        System.out.println("Below is a dump of Framework information.");
        System.out.println("Software: " + softwareName);
        System.out.println("Version: " + softwareVersion);
        System.out.println("Please report this issue to the devs. Thanks.");
//        CMWSF.state = s;
    }

}
