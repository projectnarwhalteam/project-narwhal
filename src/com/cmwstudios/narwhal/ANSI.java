package com.cmwstudios.narwhal;

public interface ANSI {
    String ANSI_RESET = "\u001B[0m";
    // --Commented out by Inspection START (12/11/18 12:05 PM):
// --Commented out by Inspection START (12/11/18 12:08 PM):
////    // --Commented out by Inspection (12/11/18 12:05 P// --Commented out by Inspection (12/11/18 12:05 PM):M):String ANSI_BLACK = "\u001B[30m";
    String ANSI_RED = "\u001B[31m";
    //// --Commented out by Inspection STOP (12/11/18 12:05 PM)
//// --Commented out by Inspection START (12/11/18 12:05 PM):
    String ANSI_GREEN = "\u001B[32m";
    String ANSI_YELLOW = "\u001B[33m";
    //// --Commented out by Inspection STOP (12/11/18 12:05 PM)
//// --Commented out by Inspection START (12/11/18 12:05 PM):
    String ANSI_BLUE = "\u001B[34m";
    String ANSI_PURPLE = "\u001B[35m";
    //// --Commented ou// --Commented out by Inspection (12/11/18 12:08 PM):t by Inspection STOP (12/11/18 12:05 PM)
    String ANSI_CYAN = "\u001B[36m";
    // --Commented out by Inspection STOP (12/11/18 12:08 PM)
    String ANSI_WHITE = "\u001B[37m";
}
