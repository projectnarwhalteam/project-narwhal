package com.cmwstudios.narwhal;

import com.bugsnag.Bugsnag;
import com.cmwstudios.narwhal.exception.LigmaException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Props {
    static Bugsnag bugsnag = new Bugsnag("07b75ed94b6e0b15e38ecae8da9069e3");
    public static String getValueString(String key) {
        Properties prop = new Properties();
        InputStream in = Props.class.getResourceAsStream("narwhal.properties");
        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
            if (getValue("AllowBugsnag")) {
            }

        }
        return prop.getProperty(key);
    }
    public static boolean getValue(String key) {
        if (getValueString(key) == "true") {
            return true;
        } else if (getValueString(key) == "false") {
            return false;
        } else {
            try {
                throw new LigmaException();
            } catch (LigmaException e) {
                if(getValue("AllowBugsnag")) {
                    bugsnag.notify(e);
                }
                e.printStackTrace();
            } finally {
                return false;
            }
        }
    }
}
