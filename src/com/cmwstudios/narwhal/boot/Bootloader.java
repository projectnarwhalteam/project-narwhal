/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.boot;

import com.cmwstudios.hiya.jash.JASH;
import com.cmwstudios.igloo.SoftwareState;
import com.cmwstudios.igloo.crashhandler.CrashHandler;
import com.cmwstudios.narwhal.boot.recovery.Recovery;
import com.cmwstudios.narwhal.boot.recovery.RecoveryReason;
import com.cmwstudios.narwhal.lg.LetsGo;
//import com.sun.istack.internal.Nullable;
//import jdk.internal.jline.internal.Nullable;

import java.io.IOException;

public class Bootloader {
    private short s;
    private int d;

    public Bootloader(BootMode bootMode, RecoveryReason recoveryReason) {
        switch (bootMode) {
            case STANDARD:
                // no, make me
            case RECOVERY:
                Recovery r = new Recovery(recoveryReason);
        }
    }
    public void letsGo(LetsGo letsGo) {
        // Here is where the paths meet. And... it's a dead end.
        switch (letsGo){
            case EVEEE:
                s = (short) Math.max(1, 3403493);
            case PIKACHU:
                s = (short) 394568;
                d = 394568 % 256;

        }
        System.out.println(s);
        System.out.println(d);
        JASH.main(null);
    }
}
