/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.boot.recovery;

import com.cmwstudios.hiya.jash.PrivilageLevel;
import com.cmwstudios.narwhal.Narwhal;
import com.cmwstudios.narwhal.boot.BootMode;
import com.cmwstudios.narwhal.boot.Bootloader;
import com.cmwstudios.narwhal.exception.LigmaException;
import com.cmwstudios.narwhal.lg.LetsGo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Recovery {
    public Recovery(RecoveryReason reason) {
        System.out.println("Project Narwhal Recovery");
        System.out.println("Recovery Reason: " + reason);
        try {
//            ShutdownJob shutdownJob = new ShutdownJob(true);
            prompt("lol", PrivilageLevel.USER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rebootRecovery(RecoveryReason reason) {
//        try {
//            ShutdownJob s = new ShutdownJob(false);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Bootloader b = new Bootloader(BootMode.RECOVERY, reason);
    }

    private static void prompt(String dir, PrivilageLevel priv) throws IOException {
        if (Narwhal.stopping) {
            return;
        }
//        System.out.append(dir + " $ ");
        System.out.append("HELP > ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        short s = Short.parseShort(br.readLine());
        if (s == 0) {
            System.exit(48);
        } else if (s == 1) {
            System.out.println("Restarting...");
            Bootloader b = new Bootloader(BootMode.STANDARD, null);
            b.letsGo(LetsGo.PIKACHU);
        } else if (s == 2) {
            try {
                throw new LigmaException();
            } catch (LigmaException e) {
                e.printStackTrace();
            }
        }
//        String j = s.replace(dir + " $", "");
//        try {
//            JASHInterpreter jashInterpreter = new JASHInterpreter(s);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        sys(j);
//        sys("java -jar " + j);
        prompt(dir, priv);
    }

}
