/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.boot.recovery;

public enum RecoveryReason {
     STARTUP_FAILURE, RESET, USER
}
