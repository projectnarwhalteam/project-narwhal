/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.exception;

class GamepadException extends Exception {
    public GamepadException() {
        System.out.println("Continue using the Wii U Gamepad.");
    }
}
