/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.exception;

public class LigmaException extends Exception {
    public LigmaException() {
        System.out.println("Goddammit, now I have ligma.");
        System.out.println("You got Ligma - test failed.");
    }

    public LigmaException(String message) {
        super(message);
    }
}
