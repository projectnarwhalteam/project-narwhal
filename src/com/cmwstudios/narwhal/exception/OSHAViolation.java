/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.exception;

public class OSHAViolation extends Exception {
        public OSHAViolation() {
                System.out.println("I'm Daddy Safety Hazard, and I'm here to say,");
                System.out.println("OSHA violations ain't the way to play!");
                System.out.println("Make sure you have your exit signs, and your loud alarms.");
                System.out.println("So in the case of an emergency, no one will be harmed!");
                System.out.println("You violated OSHA - test failed");
}
}
