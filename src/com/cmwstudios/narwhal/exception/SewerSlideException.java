/*
 * Copyright (c) 2018. Aidan Veney under Creative Commons ShareAlike 3.0 (CC BY-SA 3.0 US)
 */

package com.cmwstudios.narwhal.exception;

class SewerSlideException extends Exception {
    public SewerSlideException() {
        System.out.println("Why won't you let me die?");
    }

    public SewerSlideException(String message) {
        super(message);
    }
}
