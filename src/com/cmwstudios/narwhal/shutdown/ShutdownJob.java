package com.cmwstudios.narwhal.shutdown;

import com.cmwstudios.narwhal.Narwhal;

import static com.cmwstudios.narwhal.Narwhal.jobs;

public class ShutdownJob {
    public ShutdownJob(boolean exit) throws InterruptedException {
        Narwhal.stopping = true;
        if (jobs == 0) {
            if (exit) {
                System.exit(0);
            }
        } else {
            System.out.println("Waiting for " + jobs + "to finish...");
            while (jobs != 0) {
                System.out.append('.');
                Thread.sleep(100);
            }
            if (exit) {
                System.exit(0);
            }

        }
    }
}
